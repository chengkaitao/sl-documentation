Requests
========

.. csv-table::
	:header: "Status", "Value", "Triggered By"
	:widths: 10, 5, 30

	New Request (Validating),1,Requester submits a request via website
	New Request (Pending Admin Approval),2,Requester confirms a request via website
	New Request (Approved),3,Admin check and approve from admin panel
	Matched (Waiting for Requester's Payment),4,Traveller fulfills request on fulfill request page
	Payment Received,5,Admin check payment and update from admin panel
	Pending Pickup (Waiting for pickup),6,"traveller update status and pickup date via admin panel, else by deadline auto update"
	Pending Delivery,7,"admin arrange pickup with poslaju, get tracking code and update admin"
	Completed,8,"on deliver date, auto update status"